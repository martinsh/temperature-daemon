
#include <QCoreApplication>
#include <stdio.h>
#include "globals.h"


#include <QTextStream>
#include "pic24.h"

int main(int argc, char* argvector[]){
    g_config_name = "config/mcu.ini";
    QCoreApplication app(argc, argvector);

    app.setApplicationName("Temperature Logger");
    app.setOrganizationName("Tx");
    app.setOrganizationDomain("Tx.lv");
    QTextStream cout(stdout);
/**
    int x(4);
    int* px = 0;

    px = &x;
    cout << "x: " << x << endl;
    cout << "px: " << px << endl;
    cout << "Dereference *px: " << *px << endl;
    cout << "Addresss of pointer &px: " << &px << endl;
**/

    Pic24* pic24 = Pic24::instance();
    pic24->run();

    cout << "Go to event loop. " << endl;
    return app.exec();
}
