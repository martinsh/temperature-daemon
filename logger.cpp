#include "logger.h"
#include <QtSql>
#include <map>
#include <iostream>

// create user 'pic24'@'localhost' identified by 'pic24demopassword';
// GRANT ALL ON mcu.* TO 'pic24'@'localhost';

Logger::Logger(QObject *parent) :QObject(parent){
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("127.0.0.1");
    db.setDatabaseName("mcu");
    db.setUserName("pic24");
    db.setPassword("123456");

    _m_def_interval = 60; // default interval if none found for new sensors

    bool ok = db.open();

    if (ok)
        qDebug() << "Connected to MySQL";
    else
        qDebug() << "Connect to MySQL failed";


    //std::vector <QByteArray*> vec;
   // qDebug() << "Tables: "<< db.tables(QSql::Tables);
}

Logger::~Logger(){


}

/**
 * Expected array structure:
 * Byte 1 - command identifier (0xB6)
 * Bytes 2-9 - (8 bytes) Sensor ID
 * Bytes 10-18 (9 bytes) Scratchpad contents
 * Byte 19 - command identifier (0xB6)
 * ..and so on
 *
 * @brief Logger::log
 * @param a
 * @return
 */
int Logger::log(const QByteArray& data){

    unsigned int cursor = 0, last_dpos = data.count()-1;

    unsigned char command = (unsigned char)data.at(cursor++);
     // next position after command id



    unsigned int sensor_rom_end_pos, scratchpad_end_pos, loops = 0;


    while (command == 0xB6) {

        qDebug() << "-----------";

        // Infinite loop checker. Max limited to 100
        if (++loops > 100){
            qDebug() << "!!!Inifinite loop detected";
            break;
        }

         QByteArray sensor_rom;

        sensor_rom_end_pos = cursor+8;
        while (cursor < sensor_rom_end_pos) {

            if (cursor > last_dpos)
                break;

            sensor_rom.append(data.at(cursor++)); // increment cursor position
        }

      //  qDebug() << sensor_rom.toHex();



        /**
         * Resolution:
         * 9 bits:  0.5 C (bit 2,1,0 unused)
         * 10 bits: 0.25 C (bit 1,0 unused)
         * 11 bits: 0.125 C (bit 0 unused)
         * 12 bits: 0.0625 C (all bits used)
         *
         * We use 11 bit mode
         */
        unsigned char scratcpad[9];
        unsigned int index = 0;

        scratchpad_end_pos = cursor + sizeof scratcpad; // in the 1st iteration this is 18

        while (cursor<scratchpad_end_pos) {
            if (cursor > last_dpos)
                break;

            scratcpad[index++] = (unsigned char)data.at(cursor++); // increment cursor position
        }

        /**
        for (int y=0; y<9; ++y)
            printf("%x ", scratcpad[y]);
         **/


        /**
        if (loops == 2){
            qDebug() << "Cur: " <<  cursor;

            unsigned char v = (unsigned char)data.at(34);

            std::cout << "At: " << std::hex << (int)v;
            break;
        }
        **/



        if (cursor <= last_dpos){
            command = data.at(cursor++);
        }
        else{
            command = 0x00;
        }






//        for (unsigned int i=0; i<sizeof(scratcpad); ++i){
//           //qDebug() << scratcpad[i];
//        //    qDebug() << hex << scratcpad[i];
//        }

        unsigned char resolution_mode = (0b01100000 & scratcpad[4]) >>5;
        float multiplier = 0;
        unsigned int shift_bits = 0; // how many bits are undefined in the scratchpad

        switch (resolution_mode) {
        case 0:
            multiplier = 0.5; // 9 bit mode
            shift_bits = 3;
            break;
        case 1:
            multiplier = 0.25; // 10 bit mode
            shift_bits = 2;
            break;
        case 2:
            multiplier = 0.125; // 11 bit mode
            shift_bits = 1;
            break;
        case 3:
            multiplier = 0.0625; // 12 bit mode
            shift_bits = 0;
            break;

        default:
            qDebug() << "Error (E01): Unknown resolution mode.";
            break;
        }

        // Join LSB and MSB
        short unsigned int temperature = scratcpad[1]; // 2 bytes
        temperature <<= 8;
        temperature = temperature | scratcpad[0];

        unsigned int polarity = (temperature & 0x8000) >> 15; // Test MSbit to check temperature polarity
        polarity = polarity ^ 1; // invert bit 0: 1 - positive, 0 - subzero

        // since now we know temperature polarity, we can discard these bits
        temperature &= 0b0000011111111111;

        if (!polarity){ // invert bits if subzero. Clear SIGN bits again
            temperature = ~temperature & 0b0000011111111111;
        }

        // discard undefined bits due to conversion resolution
        temperature >>= shift_bits;



        float d_temperature = temperature*multiplier;
        if (!polarity){ // if subzero
            d_temperature = -d_temperature;
        }

       // polarity = polarity;

//        qDebug() << "#LSB" << hex << scratcpad[0];
//        qDebug() << "#MSB" << hex << scratcpad[1];

 //       qDebug() << "#polarity" << hex << temperature;
        qDebug() << "#"<< loops << ": " << d_temperature;


//        qDebug() << data.toHex();



        // Get sensor id
        QSqlQuery query;
        unsigned int min_interval;
        query.prepare("SELECT sensor_id, UNIX_TIMESTAMP(last_measure) last_measure, min_interval FROM sensors WHERE sensor_rom = :sensor_rom");
        query.bindValue(":sensor_rom", sensor_rom.toHex());

        if (!query.exec()){
            qDebug() << "SQL Failed: " << query.lastError();
            return STATUS_ERROR;
        }

        int sensor_id = 0, last_measure = 0, current_time = 0, elapsed = 0;
        if (!query.first()){

            query.prepare("INSERT INTO sensors (sensor_rom, min_interval) VALUES (:sensor_rom, :min_interval)");
            query.bindValue(":sensor_rom", sensor_rom.toHex());
            query.bindValue(":min_interval", _m_def_interval); // first insert default min_interval

            if (!query.exec())
                return STATUS_ERROR;
            qDebug() << "New Sensor added";
            sensor_id = query.lastInsertId().toInt();
            min_interval = _m_def_interval;
        }
        else{
            sensor_id = query.value("sensor_id").toInt();
            last_measure = query.value("last_measure").toInt();
            min_interval = query.value("min_interval").toInt();
        }


        if (sensor_id < 1)
            return STATUS_ERROR;


        //
        QDateTime current = QDateTime::currentDateTime();
        current_time = current.toTime_t();

        elapsed = current_time - last_measure;

        if (elapsed < min_interval)
            continue;


        query.prepare("INSERT INTO temperatures (sensor_id, reading) VALUES (:sensor_id, :reading)");
        query.bindValue(":sensor_id", sensor_id);
        query.bindValue(":reading", d_temperature);

        if (!query.exec())
            return STATUS_ERROR;

        query.prepare("UPDATE sensors SET last_measure = CURRENT_TIMESTAMP WHERE sensor_id = :sensor_id");
        query.bindValue(":sensor_id", sensor_id);
        if (!query.exec())
            return STATUS_ERROR;


        qDebug() << "Logged!";









    }














    return STATUS_OK;
}



int Logger::log_error(int err_type, QString const& more_info){
 QSqlQuery query;
 query.prepare("INSERT INTO errors (error_type, more_info) VALUES (:err_type, :more_info)");

 query.bindValue(":err_type", err_type);
 query.bindValue(":more_info", more_info);

 if (!query.exec())
     return STATUS_ERROR;

 return STATUS_OK;
}
