#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <map>

class Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(QObject *parent = 0);
    ~Logger();
    int log(const QByteArray&);
    int log_error(int, const QString &);
signals:
    
public slots:


private:

    uint _m_def_interval;

    enum LOG_STATUS
    {
        STATUS_ERROR = 0,
        STATUS_SKIPPED,
        STATUS_OK
    };





//    enum RESOLUTION_MODE{
//        BITS_9 = 0,
//        BITS_10,
//        BITS_11,
//        BITS_12
//    };
};

#endif // LOGGER_H
