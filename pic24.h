#ifndef PIC24_H
#define PIC24_H

#include <QCoreApplication>
#include <QTcpSocket>

#include <QAbstractSocket>

#include "logger.h"
class QTimer;

class Pic24:public QObject{

    Q_OBJECT
public:

    int run(void);
    static Pic24* instance(void); // Singleton

    enum Pic24Errors{
        ErrorSocketTimeout = 1,
        ErrorConnectionFailed
    };

protected:
        Pic24(QObject* parent = 0);
        ~Pic24();

private:

    struct BoardInfo{
        QString ip;
        int retry_interval;
        int timeout;
    };


    BoardInfo *_m_boardinfo;
    int m_connHandler;

    Logger* m_logger;
    QTimer* m_heartbeat, *_m_reconnect_timer;
    QTcpSocket *pSocket;


    enum STATES
    {

        ST_LOGIN = 0,
        ST_PASSWORD,
        ST_AUTHENTICATED
    };


    unsigned char _mAppState;    // State tracking member
    void processData(const QByteArray&);

public slots:
    void connected();
    void error(QAbstractSocket::SocketError);
    void stateChanged(QAbstractSocket::SocketState);
    void disconnected();
    void bytesWritten(qint64);
    void readyRead();
    void connect_to_board();
    void serverTimeout();
};


#endif // PIC24_H
