#-------------------------------------------------
#
# Project created by QtCreator 2013-04-17T21:35:19
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui
QT       += sql

TARGET = pic24tcpipQt5
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    pic24.cpp \
    logger.cpp \
    globals.cpp

HEADERS += \
    pic24.h \
    logger.h \
    globals.h

# http://qt-project.org/forums/viewthread/9208
win32{
#    LIBS += -lws2_32
}

