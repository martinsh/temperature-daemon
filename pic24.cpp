#include "pic24.h"
#include <QTcpSocket>
#include <QByteArray>
#include <QDebug>
#include <QSettings>
#include <QTimer>
#include <QString>
#include "logger.h"
#include "globals.h"

//QTextStream cout(stdout);

#ifdef Q_OS_LINUX

#endif


//#ifdef Q_OS_WIN32
//    #include <Windows.h>
//    //#include <Winsock2.h>
//#endif


Pic24::Pic24(QObject* parent):QObject(parent), _mAppState(ST_LOGIN){

    m_logger = new Logger(this);
    m_heartbeat = new QTimer(this);
    _m_reconnect_timer = new QTimer(this);

    _m_boardinfo = new BoardInfo();


    QSettings settings(g_config_name, QSettings::IniFormat);
    settings.beginGroup("board");
    _m_boardinfo->ip = settings.value("ip").toString();
    int retry_interval = settings.value("retry").toInt();
    _m_boardinfo->retry_interval = (retry_interval < 5)?10*1000:retry_interval*1000;
    int timeout = settings.value("timeout").toInt();
    _m_boardinfo->timeout = (timeout < 5)?10*1000:timeout*1000;
    settings.endGroup();

}

Pic24::~Pic24(){
    delete _m_boardinfo;
}

int Pic24::run(){

    pSocket = new QTcpSocket(this);
    connect(pSocket, SIGNAL(connected()), this, SLOT(connected()));
    connect(pSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));
    connect(pSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(stateChanged(QAbstractSocket::SocketState)));
    connect(pSocket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(pSocket, SIGNAL(readyRead()), this, SLOT(readyRead())); // https://qt-project.org/doc/qt-5.0/qtcore/qiodevice.html#readyRead
    connect(pSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64))); // https://qt-project.org/doc/qt-5.0/qtcore/qiodevice.html#bytesWritten
    connect(m_heartbeat, SIGNAL(timeout()), this, SLOT(serverTimeout()));
    connect(_m_reconnect_timer, SIGNAL(timeout()), this, SLOT(connect_to_board()));



    this->connect_to_board();

    /*
     * http://stackoverflow.com/questions/10445122/qtcpsocket-state-always-connected-even-unplugging-ethernet-wire
     * http://qt-project.org/forums/viewthread/2420
     */


    return 1;
}

/* Singleton */
Pic24* Pic24::instance(){

    static Pic24* inst = 0;
    if (inst == 0)
        inst = new Pic24(qApp);
    return inst;
}

/** SLOT (Reconnect timer) & Regular method **/
void Pic24::connect_to_board(){

    if (pSocket->state() == QAbstractSocket::UnconnectedState){
        qDebug() << "Trying to connect";
        pSocket->connectToHost(_m_boardinfo->ip, 23); // immediately returns

        if (!pSocket->waitForConnected(3000)){
            QString error = "Hardware. Connect failed.";
            m_logger->log_error(Pic24::ErrorConnectionFailed, error);
            qDebug() << "Error: Not connected" << pSocket->errorString();

        }
    }

}

/**
  Heartbeat produced timeout.
 * @brief Pic24::serverTimeout
 */
void Pic24::serverTimeout(){
    _m_reconnect_timer->start(_m_boardinfo->retry_interval);
    QString error = "Hardware. TCP timeout.";
    m_logger->log_error(Pic24::ErrorSocketTimeout, error);
    m_heartbeat->stop();
    if (pSocket->state() == QAbstractSocket::ConnectedState){
        pSocket->disconnectFromHost();
    }
}

// Slot
void Pic24::connected(){
    _m_reconnect_timer->stop();
    qDebug() << "Connected.";
    m_heartbeat->start(_m_boardinfo->timeout); // restart heartbeat
}

// Slot
void Pic24::error(QAbstractSocket::SocketError error){
    qDebug() << "Error happened";
    pSocket->disconnectFromHost();
}

void Pic24::stateChanged(QAbstractSocket::SocketState state){
    qDebug() << "State changed: " << state;
}

void Pic24::disconnected(){
    _mAppState = ST_LOGIN;
    qDebug() << "Disconnected";
}
void Pic24::bytesWritten(qint64 bytes){
    qDebug() << "We wrote " << bytes;
}
void Pic24::readyRead(){
    m_heartbeat->start(_m_boardinfo->timeout);
    QByteArray content = pSocket->readAll();

    switch (_mAppState) {

      case ST_LOGIN:
        if (content.indexOf("Login:") != -1){
            pSocket->write("admin\r\n");
            ++_mAppState;
        }

        break;

     case ST_PASSWORD:

        if (content.indexOf("Password") != -1){
            pSocket->write("microchip\r\n");
            ++_mAppState;
        }
        else
            --_mAppState;

        break;

    case ST_AUTHENTICATED:

        this->processData(content);
        break;


    default:
            _mAppState = ST_LOGIN;
        break;
    }













    //TODO parse
}



void Pic24::processData(const QByteArray& data){

    //qDebug() << data[0];

    unsigned char command = (unsigned char)data.at(0);

    switch(command){
        case 0xB6: // new temperature command

        m_logger->log(data);

        break;

    default:
         qDebug() << "Break";
        break;
    }






}
